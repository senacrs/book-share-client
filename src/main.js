import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2';
import * as VueMoment from 'vue-moment';
import VueTheMask from 'vue-the-mask'

import 'sweetalert2/dist/sweetalert2.min.css';

import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router';
import "./plugins/vee-validate";

Vue.use(VueSweetalert2);
Vue.use(VueMoment);
Vue.use(VueTheMask);

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
