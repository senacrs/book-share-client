/**
 * Format validation errors to extract messages
 * @param errors
 */
const formatValidationErrors = errors => {
  let formErrors = {};
  Object.keys(errors).forEach(error => {
    formErrors[error] = [errors[error].msg];
  });
}

export default formatValidationErrors;
