import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import Registration from './views/Registration.vue';
import Home from "@/views/Home";
import Checkouts from "@/views/Checkouts";
import Checkins from "@/views/Checkins";
import auth from "@/services/auth";
import Profile from "@/views/Profile";

Vue.use(Router);

const publicRoutes = [
  'registration',
  'login'
];

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/checkouts',
      name: 'checkouts',
      component: Checkouts
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration,
    },
    {
      path: '/checkins',
      name: 'checkins',
      component: Checkins
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    }
  ],
});

router.beforeEach((to, from, next) => {
  if (publicRoutes.indexOf(to.name) < 0 && !auth.isAuthenticated()) {
    return next('/login');
  }
  next();
});

export default router;
