import axios from './axios';

const TOKEN_KEY = "@bookshare-token";

/**
 * Get token from local storage
 * @returns {string}
 */
const getToken = () => localStorage.getItem(TOKEN_KEY);

/**
 * Check if user is authenticated
 * @returns {boolean}
 */
const isAuthenticated = () => getToken() !== null;

/**
 * Set token into local storage
 * @param token
 */
const setToken = token => {
  localStorage.setItem(TOKEN_KEY, token);
};

/**
 * Remove local storage token
 */
const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
};

/**
 * Login user
 * @param data
 * @returns {*}
 */
const login = data => axios.post('login', data)
  .then(({ data }) => {
    setToken((data.token));
    return data;
  })
  .catch(error => {
    throw error.response.data;
  });

export default {
  TOKEN_KEY,
  login,
  isAuthenticated,
  getToken,
  logout,
};
