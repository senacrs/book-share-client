import axios from './axios';

/**
 * Checkin book
 * @param data
 * @returns {*}
 */
const create = checkoutId => {
  return axios.post('checkins', { checkoutId })
    .catch(error => {
      throw error.response.data;
    });
}

/**
 * Get checkins list
 * @param data
 * @returns {*}
 */
const getAll = () => {
  return axios.get('checkins')
    .then(response => response.data)
    .catch(error => {
      throw error.response.data;
    });
}

export default {
  create,
  getAll
};
