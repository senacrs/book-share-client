import axios from 'axios';

const cepApi = axios.create({
  baseURL: 'https://viacep.com.br/ws',
});

/**
 * Get address by cep
 * @param cep
 * @returns {*}
 */
const getAddressByCep = cep => {
  return cepApi.get(`${cep}/json`)
    .then(response => response.data)
    .then(data => ({
      cep: data.cep,
      street: data.logradouro,
      city: data.localidade,
      state: data.uf,
      complement: data.complemento,
      district: data.bairro
    }));
}

export default  {
  getAddressByCep
}
