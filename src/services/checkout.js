import axios from './axios';

/**
 * Get checkouts list
 * @param data
 * @returns {*}
 */
const getAll = () => {
  return axios.get('checkouts')
    .then(response => response.data)
    .catch(error => {
      throw error.response.data;
    });
}

/**
 * Checkout book
 * @param data
 * @returns {*}
 */
const create = data => {
  return axios.post('checkouts', data)
    .catch(error => {
      throw error.response.data;
    });
}

export default {
  getAll,
  create
};
