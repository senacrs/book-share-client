import axios from 'axios';
import auth from '@/services/auth';

const api = axios.create({
  baseURL: 'http://localhost/api',
});

/**
 * Token request interceptor
 */
api.interceptors.request.use(async config => {
  const token = auth.getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

/**
 * Unauthorized response interceptor
 */

// Add a 401 response interceptor
api.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status) {
    auth.logout();
  } else {
    return Promise.reject(error);
  }
});

export default api;
