import axios from './axios';

/**
 * Register new user
 * @param data
 * @returns {*}
 */
const register = data => axios.post('register', data)
  .catch(error => {
    throw error.response.data;
  });

/**
 * Get user profile
 * @returns {Q.Promise<any> | PromiseLike<any>}
 */
const profile = () => {
  return axios.get('me')
    .then(response => response.data)
}

/**
 * Update user data
 * @param data
 * @returns {Promise<T>}
 */
const update = data => {
  return axios.put('me', data)
    .catch(error => {
      throw error.response.data
    });
}

export default {
  profile,
  register,
  update
};
