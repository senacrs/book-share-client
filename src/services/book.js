import axios from './axios';


/**
 * Get book list
 * @param data
 * @returns {*}
 */
const getAll = (filters) => {
  return axios.get('books', { params: filters })
    .then(response => response.data)
    .catch(error => {
      throw error.response.data;
    });
}

export default {
  getAll
};
